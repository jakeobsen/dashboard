<?php
include('config.php');
include('db.php');

/* $result = $db->query("SELECT * FROM `options`;");
if($result->num_rows > 0) { while ( $row = $result->fetch_assoc() ) {
	$conf[$row['var']] = $row['val'];
}} */

// Determine which module functions to call
switch ($_GET['module']) {
	case 'bookmarks':
		include('inc/module/bookmarks/bookmarks-actions.php');
    header('Location: '.$_SERVER['SCRIPT_NAME']);
		break;
}
