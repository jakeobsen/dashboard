<div class="widget-form bookmarks-form">

  <fieldset>
    <legend>Add bookmark</legend>
    <form method="post" action="?module=bookmarks&action=add-bookmark">
      <select name="bookmarks-folder">
        <?=bookmarks_option_folders($db);?>
      </select>
      <input type="text" name="bookmarks-title" placeholder="Title" maxlength="256" />
      <input type="text" name="bookmarks-url" placeholder="URL" maxlength="512" />
      <button type="submit" name="bookmarks-add-bookmark">Add Bookmark</button>
    </form>
  </fieldset>

  <fieldset>
    <legend>Add folder</legend>
    <form method="post" action="?module=bookmarks&action=add-bookmark-folder">
      <input type="text" name="bookmarks-folder" placeholder="Foldername" maxlength="256" />
      <button type="submit" name="bookmarks-add-folder">Add Folder</button>
    </form>
  </fieldset>

  <fieldset>
    <legend>Delete folder</legend>
    <form method="post" action="?module=bookmarks&action=delete-bookmark-folder">
      <select name="bookmarks-folder">
        <?=bookmarks_option_folders($db);?>
      </select>
      <button type="submit" name="bookmarks-delete-folder">Delete Folder</button>
    </form>
  </fieldset>

  <div class="clearFix"></div>
</div>
