<?php
// Get all the data we need in one query
$query = "SELECT
	`b`.`id`,
	`f`.`folder`,
	`b`.`title`,
	`b`.`url`

FROM
`bookmarks` `b`,
`bookmarks_folders` `f`

WHERE
	`b`.`folder` = `f`.`id`

ORDER BY `f`.`folder` ASC,  `b`.`title` ASC";

// Get data into data array
$result = $db->query($query);
if($result->num_rows > 0) {
	while ( $row = $result->fetch_assoc() ) {
  $bookmarks_data[$row['folder']][] = [
		htmlspecialchars($row['id']),
		htmlspecialchars($row['title']),
		htmlspecialchars($row['url'])
	];
	}
}

// Output from data array
foreach ($bookmarks_data as $folder => $bookmarks) {
  echo '<div class="bookmarks-folder">';
  echo '<div class="widget-header"><h3>'.$folder.'</h3></div><ul>';
  foreach ($bookmarks as $bookmark) {
    echo '<li><a href="'.$bookmark[2].'">'.$bookmark[1].'</a> <a href="?module=bookmarks&action=delete-bookmark&bookmarks-id='.$bookmark[0].'" class="delete">X</a></li>';
  }
  echo "</ul></div>";
}
