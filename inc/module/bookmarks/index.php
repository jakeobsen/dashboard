<?php include('bookmarks-functions.php'); ?>

<div class="widget-header">
  <h3>Bookmarks</h3>
</div>

<div class="widget-content">
  <?php include('bookmarks-form.php'); ?>
</div>

<div class="line"></div>

<div class="widget-content">
  <?php include('bookmarks-list.php'); ?>
  <div class="clearFix"></div>
</div>
