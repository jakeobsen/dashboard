<?php

// Determine which action to perform
switch ($_GET['action']) {
  case 'add-bookmark':
    add_bookmark($db);
    break;
  case 'add-bookmark-folder':
    add_bookmark_folder($db);
    break;
  case 'delete-bookmark':
    delete_bookmark($db);
    break;
  case 'delete-bookmark-folder':
    delete_bookmark_folder($db);
    break;
}

// Add bookmark to folder
function add_bookmark($db) {
  $folder = $db->real_escape_string($_POST['bookmarks-folder']);
  $title  = $db->real_escape_string($_POST['bookmarks-title']);
  $url    = $db->real_escape_string($_POST['bookmarks-url']);
  $db->query("INSERT INTO `dashboard`.`bookmarks` (`folder`, `title`, `url`) VALUES ('$folder', '$title', '$url');");
}

// Create bookmark folder
function add_bookmark_folder($db) {
  $folder = $db->real_escape_string($_POST['bookmarks-folder']);
  $db->query("INSERT INTO `dashboard`.`bookmarks_folders` (`folder`) VALUES ('$folder');");
}

// Delete bookmark
function delete_bookmark($db) {
  $id = $db->real_escape_string($_GET['bookmarks-id']);
  $db->query("DELETE FROM `dashboard`.`bookmarks` WHERE  `id`='$id';");
}

// Delete bookmark folder and it's contents
function delete_bookmark_folder($db) {
  $id = $db->real_escape_string($_POST['bookmarks-folder']);
  $db->query("DELETE FROM `dashboard`.`bookmarks` WHERE  `folder`='$id';");
  $db->query("DELETE FROM `dashboard`.`bookmarks_folders` WHERE  `id`='$id';");
}
