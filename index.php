<?php include('inc/php/system.php'); ?><!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
  <link href="inc/css/style.css" type="text/css" rel="stylesheet" />
	<link href="inc/module/bookmarks/style.css" type="text/css" rel="stylesheet" />
	<link href="inc/module/search/style.css" type="text/css" rel="stylesheet" />
	<title>Dashboard</title>
</head>
<body>

<div id="header">
  <h1>Dashboard</h1>
</div>

<div id="dashboard">
	<div class="widget-column">
		<div class="widget-innerpadding">
			<div class="widget"><?php include('inc/module/search/index.php'); ?></div>
		</div>
	</div>

	<div class="widget-column">
		<div class="widget-innerpadding">
			<div class="widget"><?php include('inc/module/bookmarks/index.php'); ?></div>
		</div>
	</div>
</div>

<div id="footer">
	<p>Dashboard v0.1 &copy; 2018 Morten Jakobsen</p>
</div>

</body>
</html>
