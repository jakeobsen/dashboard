# Dashboard

Simple dashboard system that currently only had a bookmark function.
More features will (maybe) be added later as I need them.

Feel free to use this on your own.

Contributions are welcome

## Video

[![Video Demo](https://img.youtube.com/vi/PoYKLJ5mTiU/0.jpg)](https://www.youtube.com/watch?v=PoYKLJ5mTiU) 

## Installation

To install this version, import the database.sql file into a database then
update the inc/php/config.php file with the mysql details. An example
config.php file is provided.

The system is fairly simple, so I'm not going to explain things in detail. Read
the source code and try the system on it's webpage.
